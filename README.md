# GitLab Moonshot

This application is dirivative of [moon.js](https://github.com/siravan/moonjs) (original [README](./orig_README.md)) to make the Apollo Guidance Computer (AGC) control GitLab CI/CD.

## Why tho?

At first it was just a fun project to honor the 50th anniversary of Apollo 11.  And now...well it's still just that for now.

## How

Using [GitLab CI/CD]() itself (see the [.gitlab-ci.yml](./.gitlab-ci.yml)) to build it is a little meta, but that's okay.

### Getting Started

Moon.js itself is a port of the original AGC project in C.  So we'll start with the prerequisties one needs to build this (on a Mac #sorrynotsorry):

* 